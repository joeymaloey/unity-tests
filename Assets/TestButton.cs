﻿using UnityEngine;
using System.Collections;

public class TestButton : MonoBehaviour
{
	public GameObject tempObject;
	public GameObject tempObject2;
	public LookAtLatestSlice camera;
	GameObject [] anotherObject;
	GameObject [] fallingObjects;
	int counter = 0;
	public void reset ()
	{
		timeOut = false;
		for (int x = 1; x < anotherObject.Length; x ++) 
			Destroy (anotherObject [x]);
		for (int x = 1; x < fallingObjects.Length; x ++) 
			Destroy (fallingObjects [x]);
		anotherObject = new GameObject[1];
		fallingObjects = new GameObject[1];
		anotherObject [0] = tempObject;
		fallingObjects [0] = tempObject2;
		anotherObject[0].SetActive (true);
		anotherObject[0].SendMessage ("resumeMoving");
		fallingObjects [0].SetActive (true);
		counter = 0;
		anotherObject[0].SendMessage ("setCurrentNumber", counter);
		anotherObject[0].transform.localScale = new Vector3 (1, 1, 10);
		anotherObject[0].transform.localPosition = new Vector3 (0, 0, 0);
		fallingObjects [0].transform.localScale = new Vector3 (0, 0, 0);
		counter = 0;
	}
	void Start ()
	{
		counter = 0;
		anotherObject = new GameObject[1];
		fallingObjects = new GameObject[1];
		anotherObject [0] = tempObject;
		fallingObjects [0] = tempObject2;
		anotherObject[counter].SetActive (true);
		anotherObject[counter].SendMessage ("setCurrentNumber", counter);
		//anotherObject[0].SendMessage ("resumeMoving");
	}
	void finish ()
	{
		timeOut = true;
		anotherObject[counter].SendMessage ("stopMoving");
	}
	bool timeOut = false;
	void OnClick ()
	{
		if (!timeOut) {
			anotherObject[counter].SendMessage ("stopMoving");
			if (counter == 0) {
				counter ++;
				camera.thingToLookAt = new Vector3 (-10, 7+counter, 5);
				camera.anglesToGoTo = new Vector3 (29 + counter, 90, 0);
				
				GameObject [] temp = new GameObject[anotherObject.Length];
				for (int x = 0; x < temp.Length; x ++)
					temp [x] = anotherObject [x];
				anotherObject = new GameObject[temp.Length + 1];
				anotherObject [temp.Length] = Object.Instantiate (temp [temp.Length - 1]);
				for (int x = 0; x < temp.Length; x ++)
					anotherObject [x] = temp [x];
				
				//anotherObject = Object.Instantiate (anotherObject);
				anotherObject[counter].SendMessage ("setCurrentNumber", counter);
				anotherObject[counter].transform.localScale = new Vector3 (1, 1, 10);
				anotherObject[counter].transform.localPosition = new Vector3 (0, counter, 10);
			} else {
				float z1 = anotherObject[counter].transform.localScale.z;
				float centerPoint1 = anotherObject[counter].transform.localPosition.z;
				float left1 = centerPoint1 - z1 / 2;
				float right1 = centerPoint1 + z1 / 2;
				
				float z2 = anotherObject [counter-1].transform.localScale.z;
				float centerPoint2 = anotherObject[counter-1].transform.localPosition.z;
				float left2 = centerPoint2 - z2 / 2;
				float right2 = centerPoint2 + z2 / 2;
				
				float mainLeft = left1;
				float mainRight = right1;
				
				if (mainLeft < left2)
					mainLeft = left2;
				if (mainRight > right2)
					mainRight = right2;
				
				counter ++;
				camera.thingToLookAt = new Vector3 (-10, 7+counter, 5);
				camera.anglesToGoTo = new Vector3 (29 + counter, 90, 0);
				
				GameObject [] temp = new GameObject[anotherObject.Length];
				for (int x = 0; x < temp.Length; x ++)
					temp [x] = anotherObject [x];
				anotherObject = new GameObject[temp.Length + 1];
				anotherObject [temp.Length] = Object.Instantiate (temp [temp.Length - 1]);
				for (int x = 0; x < temp.Length; x ++)
					anotherObject [x] = temp [x];
				
				GameObject [] temp2 = new GameObject[fallingObjects.Length];
				for (int x = 0; x < temp2.Length; x ++)
					temp2 [x] = fallingObjects [x];
				fallingObjects = new GameObject[fallingObjects.Length + 1];
				fallingObjects [temp2.Length] = Object.Instantiate (temp2 [temp2.Length - 1]);
				for (int x = 0; x < temp2.Length; x ++)
					fallingObjects [x] = temp2 [x];
				
				
				if (mainLeft < mainRight && mainLeft >= left2 && mainRight <= right2) {
					anotherObject[counter-1].transform.localScale = new Vector3 (1, 1, mainRight - mainLeft);
					anotherObject[counter-1].transform.localPosition = new Vector3 (0, counter - 1, (mainRight + mainLeft) / 2);
					
					if (right1 > right2)
					{
						fallingObjects [fallingObjects.Length - 1].transform.localScale = new Vector3 (1, 1, right2 - right1);
						fallingObjects [fallingObjects.Length - 1].transform.localPosition = new Vector3 (0, counter - 1, (right2 + right1) / 2);
					}
					else if (left1 < left2)
					{
						fallingObjects [fallingObjects.Length - 1].transform.localScale = new Vector3 (1, 1, left1 - left2);
						fallingObjects [fallingObjects.Length - 1].transform.localPosition = new Vector3 (0, counter - 1, (left1 + left2) / 2);
					}
					fallingObjects [fallingObjects.Length - 1].transform.localEulerAngles = new Vector3 (0, 0, 0);
					fallingObjects [fallingObjects.Length - 1].GetComponent <Renderer>().material.color = Color.gray;
					fallingObjects [fallingObjects.Length - 1].SetActive (true);
					
					anotherObject[counter].SetActive (true);
					anotherObject[counter].SendMessage ("setCurrentNumber", counter);
					if (counter % 2 == 0) {
						anotherObject[counter].transform.localScale = new Vector3 (1, 1, mainRight - mainLeft);
						anotherObject[counter].transform.localPosition = new Vector3 (0, counter, -5 + Mathf.Abs (mainRight - mainLeft) / 2);
					} else {
						anotherObject[counter].transform.localScale = new Vector3 (1, 1, mainRight - mainLeft);
						anotherObject[counter].transform.localPosition = new Vector3 (0, counter, 15 - Mathf.Abs (mainRight - mainLeft) / 2);
					}
				} else {
					fallingObjects [fallingObjects.Length - 1].transform.localScale = new Vector3 (1, 1, right1 - left1);
					fallingObjects [fallingObjects.Length - 1].transform.localPosition = new Vector3 (0, counter - 1, (right1 + left1) / 2);
					fallingObjects [fallingObjects.Length - 1].transform.localEulerAngles = new Vector3 (0, 0, 0);
					fallingObjects [fallingObjects.Length - 1].GetComponent <Renderer>().material.color = Color.gray;
					fallingObjects [fallingObjects.Length - 1].SetActive (true);
					anotherObject[counter-1].transform.localScale = new Vector3 (0, 0, 0);
					anotherObject[counter].transform.localScale = new Vector3 (0, 0, 0);
					finish ();
				}
			}
		}
	}
}