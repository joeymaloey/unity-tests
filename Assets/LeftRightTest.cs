﻿using UnityEngine;
using System.Collections;

public class LeftRightTest : MonoBehaviour {
	int minSpeed = 3;
	float incSpeed = 1.5f;
	int currentNumber;
	bool move = true;
	bool direction = true;
	void Start ()
	{
		GetComponent <Renderer>().material.color = Color.red;
	}
	void FixedUpdate ()
	{
		if (move) {
			if (direction)
			{
				transform.Translate (0f, 0f, (minSpeed + currentNumber * incSpeed) * Time.deltaTime);
				if (transform.localPosition.z + transform.localScale.z/2 > 15)
					direction = !direction;
			}
			else
			{
				transform.Translate (0f, 0f, -(minSpeed + currentNumber * incSpeed) * Time.deltaTime);
				if (transform.localPosition.z - transform.localScale.z/2 < -5)
					direction = !direction;
			}
		}
	}
	void stopMoving ()
	{
		move = false;
		GetComponent <Renderer>().material.color = Color.blue;
	}
	void resumeMoving ()
	{
		move = true;
		GetComponent <Renderer>().material.color = Color.red;
	}
	void setCurrentNumber (int n)
	{
		currentNumber = n;
	}
	//void setSize (
}