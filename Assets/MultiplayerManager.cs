using UnityEngine;
using System.Collections;

using GooglePlayGames;
using GooglePlayGames.BasicApi;
using UnityEngine.SocialPlatforms;

public class MultiplayerManager : GooglePlayGames.BasicApi.Multiplayer.RealTimeMultiplayerListener {
	/*public void Start (){
		GooglePlayGames.PlayGamesPlatform.Activate();
	}*/

	// Use this for initialization
	public void Start () {
		InvitationReceivedDelegate thingy = new InvitationReceivedDelegate (DoThingies);
		
		//MatchDelegate mMD = new MatchDelegate ();
		PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder()//ohhhhh. only use this for multiplayer invitations and turn notifications. 
			// enables saving game progress.
			.EnableSavedGames()
				// registers a callback to handle game invitations received while the game is not running.
				.WithInvitationDelegate(thingy)
				// registers a callback for turn based match notifications received while the
				// game is not running.
				.WithMatchDelegate(DoOtherThingies)
				.Build();
		
		PlayGamesPlatform.InitializeInstance(config);
		// recommended for debugging:
		PlayGamesPlatform.DebugLogEnabled = true;
		// Activate the Google Play Games platform
		PlayGamesPlatform.Activate();
		
		const int MinOpponents = 1, MaxOpponents = 1;
		const int GameVariant = 0;
		
		PlayGamesPlatform.Instance.RealTime.CreateQuickGame(MinOpponents, MaxOpponents,
		                                                    GameVariant, this);
	}
	
	void DoThingies (GooglePlayGames.BasicApi.Multiplayer.Invitation what, bool ok){
		//print ("nyan");
	}
	void DoOtherThingies (GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch what, bool ok){
		//print ("nyan");
	}
	
	public void OnRoomConnected(bool success) {
		if (success) {
			//print ("Yay?");
			
		} else {
			//print ("IDKAY");
		}
	}
	// Update is called once per frame
	void Update () {
		
	}
	
	public GameObject mainmenu;
	
	public void get_RealTime (){
	}
	public void CreateQuickMatch ()
	{
		PlayGamesPlatform.Instance.RealTime.CreateQuickGame (1, 1, 0, this);
	}
	
	public void OnRoomSetupProgress (float thing){
	}
	public void OnRealTimeMessageReceived (bool thing1, string thing2, byte[] thing3){
		//print ("huh");
	}
	
	public void OnPeersDisconnected(string[]thing){}
	public void OnPeersConnected(string[]thing){}
	public void OnParticipantLeft (GooglePlayGames.BasicApi.Multiplayer.Participant thing){}
	public void OnLeftRoom (){}
}

