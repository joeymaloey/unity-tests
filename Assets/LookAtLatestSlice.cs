﻿using UnityEngine;
using System.Collections;

public class LookAtLatestSlice : MonoBehaviour {
	
	public void reset ()
	{
		transform.position = new Vector3 (-10, 7, 5);
		transform.eulerAngles = new Vector3 (29, 90, 0);
		thingToLookAt = transform.position;
		anglesToGoTo = transform.eulerAngles;
	}
	// Use this for initialization
	void Start () {
		transform.position = new Vector3 (-10, 7, 5);
		transform.eulerAngles = new Vector3 (29, 90, 0);
		thingToLookAt = transform.position;
		anglesToGoTo = transform.eulerAngles;
	}
	
	public Vector3 thingToLookAt;
	public Vector3 anglesToGoTo;
	float smoothTime = 1F;
	Vector3 velocity2 = Vector3.zero;
	Vector3 velocity = Vector3.zero;

	void Update ()
	{
		transform.position = Vector3.SmoothDamp (transform.position, thingToLookAt, ref velocity, smoothTime);
		transform.eulerAngles = Vector3.SmoothDamp (transform.eulerAngles, anglesToGoTo, ref velocity2, smoothTime);
	}
}
