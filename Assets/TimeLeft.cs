﻿using UnityEngine;
using System.Collections;
using System;

public class TimeLeft : MonoBehaviour {
	int timeLeft = 10;

	// Use this for initialization

	public void reset ()
	{
		transform.position = new Vector3 (94.6f, -10.4f, 14.5f);
		done = false;
		lastTime = DateTime.UtcNow.Ticks;
		//timeLeft = 10;
	}
	public void finish ()
	{
		done = true;
	}
	void Start () {
		lastTime = DateTime.UtcNow.Ticks;
		timeLeft = 10;
		finish ();
	}
	DateTime thing;
	long lastTime;
	bool done = false;
	// Update is called once per frame
	void Update () {
		if (!done) {
			//print (DateTime.UtcNow.Ticks);
			timeLeft = (10 - (int)((DateTime.UtcNow.Ticks - lastTime) / 10000000.0));
			if (timeLeft < 0) {
				testButton.BroadcastMessage ("finish");
				transform.position = new Vector3 (94.6f, -10.4f, 32.7f);
				GetComponent <TextMesh> ().text = "Time up!";
				done = true;
			} else
				GetComponent<TextMesh> ().text = timeLeft + "";
		}
	}
	public MonoBehaviour testButton;
}
