﻿using UnityEngine;
using System.Collections;
using System;

public class fps : MonoBehaviour {

	// Use this for initialization
	void Start () {
		lastTime = DateTime.UtcNow.Ticks;
	}
	
	// Update is called once per frame

	long lastTime;
	void Update () {
		GetComponent <TextMesh> ().text = "fps: " + ((int)(1/((DateTime.UtcNow.Ticks - lastTime) / 10000000.0)));
		lastTime = DateTime.UtcNow.Ticks;
	}
}
